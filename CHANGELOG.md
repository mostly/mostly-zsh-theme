# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/).

## [Unreleased]
- Async for git status.
- Move emoji's into a data structure and out of that infernal set of conditional statements.
- Fade-style colouring for return code

## [1.1.0] - 2018-07-04

### Prezto implementation
- Implemented python virtualenv support

## [1.0.1] - 2018-06-14

### Prezto implementation
- Git status implemented using prezto modules.
- VI-mode display implemented on *PROMPT* instead of RPROMPT and takes place of User symbol display. Implemented with prezto modules.
- Theme takes an argument with 3 handled options: *candy* implements Molokai colours, *simple* implements base RGB colours, anything else uses standard system colours. If a non-256-colour terminal is detected then system colours are used independant of the argument provided.
- Many more git status events reported.

## [1.0.0] - 2018-06-13

### Added

- Candy coloured variant based on Molokai.
- Terminal VI-mode display.
- Root special character display.
- ssh login info.
- Git status.
- **oh-my-zsh** implementation.
- Emoji return codes.
- Battery-monitor.
- Changelog.

[Unreleased]: https//gitlab.com/mostly/hecate/compare/1.0.1...HEAD
[1.0.1]: https://gitlab.com/mostly/hecate/compare/1.0.0...1.0.1
