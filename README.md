# mostly zsh theme

![mostly zsh theme](images/project-title.jpg)

## What is it?

This is a flashy, highly impractical *zsh* theme that I really enjoyed creating and now cannot work without. It was initially implemented in *oh-my-zsh* but has since migrated to *prezto*. There are slight differences in the features offered by each implementation and they can be seen as below:

![Presto implementation vs oh-my-zsh implementation](images/features.png)

### Feature-list

* Randomized colour + randomized face emoji return codes.
* Git status when in git working folders including branch names, status and sha-1.
* Battery status on laptops or indication you are running on mains power.
* Vi-mode status indicator when using *zsh* in **vi-mode**.
* SSH terminal login indicator.
* Special user symbol when logged in as root.
* Preset colour themes on 256-colour terminals.

The prompt is probably too busy for small terminals of width 80 or less... but it works for me. I chose a terminal width of 80 for all screenshots, firstly to minimize the project size when cloning, but mainly to show the theme at its worst.

## How do I use it?

### Acquiring

Hecate can currently only be obtained through gitlab:

1. For git users:

    git clone https://gitlab.com/mostly/mostly-zsh-theme.git

2. For non-git users, download a zip and extract it.

### Installing for *oh-my-zsh*

![Candy coloured theme for oh-my-zsh](images/desktop-oh-my-zsh-hardcandy.png)

The top level folder containing **this** README file will be referred to as the *project root folder*.

This theme does not make use of any additional packages beyond *oh-my-zsh*. Installing the theme requires you to copy the theme to your *oh-my-zsh*-installation-folder which can reside in different locations depending on your installation.

Change to the *project root folder* and type:

    cp oh-my-zsh/* $ZSH/themes

The **$ZSH** environment variable should have been set up during the installation of *oh-my-zsh* to point to your *oh-my-zsh*-installation-folder. This should be visible in your *~/.zshrc* file.

Now activate the mostly theme, open *~/.zshrc* in your editor of choice and modify the following line:

    export ZSH_THEME="mostly"

for the theme using your default terminal colours. Or:

    export ZSH_THEME="mostly-hard-candy"

for the theme using candy colours. Open a new terminal and the theme should be active.

### Installing for *Prezto*

![Candy coloured theme for prezto](images/desktop-prezto-candy.png)

The top level folder containing **this** README file will be referred to as the *project root folder*.

This theme makes use of the *FULL* installation of *Prezto*, specifically the external module *async*. Installing the theme requires you to copy the theme to your *Prezto*-installation-folder which can reside in different locations depending on your installation.

Change to the *project root folder* and type:

    cp prezto/* $ZPREZTODIR/modules/prompt/functions

The **$ZPREZTODIR** environment variable should have been set up during the installation of *Prezto* to point to your *Prezto*-installation-folder.

Now activate the mostly theme, open *~/.zpreztorc* in your editor of choice and modify the following line:

    zstyle ':prezto:module:prompt' theme 'mostly'

for the theme using your default terminal colours. Or:

    zstyle ':prezto:module:prompt' theme 'mostly' 'candy'

for the theme using candy colours. Open a new terminal and the theme should be active.

## Screenshots

### Prezto

Laptop candy
    zstyle ':prezto:module:prompt' theme 'mostly' 'candy'

![candy](images/laptop-prezto-candy.png)

Desktop simple 
    zstyle ':prezto:module:prompt' theme 'mostly' 'candy'

![simple](images/desktop-prezto-simple.png)

### oh-my-zsh

Desktop candy

![hardcandy](images/desktop-oh-my-zsh-hardcandy.png)

## Troubleshooting

### Colour themes not loading

The *prezto* implementation checks the value of the **$TERM** variable. If you are unable to load a coloured variant this variable might not be indicating that you are running in a 256-colour enabled terminal. Test this by:

    export TERM=xterm-256color
    prompt -s mostly candy

If the candy colour works then you need to add the export line to your *.zshenv* file.
