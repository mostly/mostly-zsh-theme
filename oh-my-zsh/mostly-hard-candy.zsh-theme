# MOSTLY ZSH THEME

local git_prefix="%{$FG[254]%} : ‹ %{$FG[081]%}"
local git_ahead="%{$FG[197]%}↑"
local git_staged="%{$FG[148]%}●"
local git_unstaged="%{$FG[160]%}●"
local git_untracked="%{$FG[254]%}●"
local git_unmerged="%{$FG[160]%}✕"
local git_suffix=" %{$FG[254]%}›%{$reset_color%}"

function mostly_git_prompt() {
  tester=$(git rev-parse --git-dir 2> /dev/null) || return
  
  mostly_git_index=$(git status --porcelain 2> /dev/null)
  mostly_git_status=""

  # is branch ahead?
  if $(echo "$(git log origin/$(git_current_branch)..HEAD 2> /dev/null)" | grep '^commit' &> /dev/null); then
    mostly_git_status="$mostly_git_status${git_ahead}"
  fi

  # is anything staged?
  if $(echo "$mostly_git_index" | command grep -E -e '^(D[ M]|[MARC][ MD]) ' &> /dev/null); then
    mostly_git_status="$mostly_git_status${git_staged}"
  fi

  # is anything unstaged?
  if $(echo "$mostly_git_index" | command grep -E -e '^[ MARC][MD] ' &> /dev/null); then
    mostly_git_status="$mostly_git_status${git_unstaged}"
  fi

  # is anything untracked?
  if $(echo "$mostly_git_index" | grep '^?? ' &> /dev/null); then
    mostly_git_status="$mostly_git_status${git_untracked}"
  fi

  # is anything unmerged?
  if $(echo "$mostly_git_index" | command grep -E -e '^(A[AU]|D[DU]|U[ADU]) ' &> /dev/null); then
    mostly_git_status="$mostly_git_status${git_unmerged}"
  fi

  if [[ -n $mostly_git_status ]]; then
    mostly_git_status=" $mostly_git_status"
  fi

  echo "${git_prefix}$(mostly_current_git_branch)$mostly_git_status${git_suffix}"
}

function mostly_current_git_branch() {
  if [[ "master" == $(git_current_branch) ]]; then
    echo -n "%{$FG_BOLD[208]%}master"
  else
    echo $(git_current_branch || echo "(no branch)")
  fi
}

function mostly_negative_return_code() {
  val=$(bash -c 'echo $RANDOM')
  case $(( $val % 3 )) in
    0 ) mostly_ret_colour="%{$FG[160]%}";                   ;;
    1 ) mostly_ret_colour="%{$FG[197]%}";               ;;
    2 ) mostly_ret_colour="%{$FG[208]%}";                ;;
  esac
  val=$(bash -c 'echo $RANDOM')
  case $(( $val % 17 )) in
    0 ) echo "${mostly_ret_colour}(¬_¬%)%{$reset_color%}";                ;;
    1 ) echo "${mostly_ret_colour}(§_§%)%{$reset_color%}";                ;;
    2 ) echo "${mostly_ret_colour}(._.%)%{$reset_color%}";                ;;
    3 ) echo "${mostly_ret_colour}(x_x%)%{$reset_color%}";                ;;
    4 ) echo "${mostly_ret_colour}(°□°%)%{$reset_color%}";                ;;
    5 ) echo "${mostly_ret_colour}(⊙_☉%)%{$reset_color%}";                ;;
    6 ) echo "${mostly_ret_colour}(ò_ó%)%{$reset_color%}";                ;;
    7 ) echo "${mostly_ret_colour}(•̀_•́%)%{$reset_color%}";                ;;
    8 ) echo "${mostly_ret_colour}(º-°%)%{$reset_color%}";                ;;
    9 ) echo "${mostly_ret_colour}(>▂<%)%{$reset_color%}";                ;;
    10 ) echo "${mostly_ret_colour}(╥_╥%)%{$reset_color%}";               ;;
    11 ) echo "${mostly_ret_colour}(ᗒᗣᗕ%)%{$reset_color%}";               ;;
    12 ) echo "${mostly_ret_colour}(⊙.☉%)%{$reset_color%}";               ;;
    13 ) echo "${mostly_ret_colour}(◣_◢%)%{$reset_color%}";               ;;
    14 ) echo "${mostly_ret_colour}(•̀o•́%)%{$reset_color%}";               ;;
    15 ) echo "${mostly_ret_colour}(ʘᗝʘ%)%{$reset_color%}";               ;;
    16 ) echo "${mostly_ret_colour}(•ิ_•ิ%)%{$reset_color%}";               ;;
  esac
}

function mostly_positive_return_code() {
  val=$(bash -c 'echo $RANDOM')
  case $(( $val % 3 )) in
    0 ) mostly_ret_colour="%{$FG[148]%}";              ;;
    1 ) mostly_ret_colour="%{$FG[177]%}";               ;;
    2 ) mostly_ret_colour="%{$FG[081]%}";               ;;
  esac
  val=$(bash -c 'echo $RANDOM')
  case $(( $val % 17 )) in
    0 ) echo "${mostly_ret_colour}(·O·%)%{$reset_color%}";               ;;
    1 ) echo "${mostly_ret_colour}(^*^%)%{$reset_color%}";               ;;
    2 ) echo "${mostly_ret_colour}(°~°%)%{$reset_color%}";               ;;
    3 ) echo "${mostly_ret_colour}(*.*%)%{$reset_color%}";               ;;
    4 ) echo "${mostly_ret_colour}ʕ•ᴥ•ʔ%{$reset_color%}";                ;;
    5 ) echo "${mostly_ret_colour}(•̀ᴗ•́%)%{$reset_color%}";               ;;
    6 ) echo "${mostly_ret_colour}(˘³˘%)%{$reset_color%}";               ;;
    7 ) echo "${mostly_ret_colour}(^∇^%)%{$reset_color%}";               ;;
    8 ) echo "${mostly_ret_colour}(‾▿‾%)%{$reset_color%}";               ;;
    9 ) echo "${mostly_ret_colour}(◕‿◕%)%{$reset_color%}";               ;;
    10 ) echo "${mostly_ret_colour}(•‿•%)%{$reset_color%}";              ;;
    11 ) echo "${mostly_ret_colour}(ᴗ˳ᴗ%)%{$reset_color%}";              ;;
    12 ) echo "${mostly_ret_colour}(^ᗜ^%)%{$reset_color%}";              ;;
    13 ) echo "${mostly_ret_colour}(•ε•%)%{$reset_color%}";              ;;
    14 ) echo "${mostly_ret_colour}(¬‿¬%)%{$reset_color%}";              ;;
    15 ) echo "${mostly_ret_colour}(°◇°%)%{$reset_color%}";              ;;
    16 ) echo "${mostly_ret_colour}('ᴗ'%)%{$reset_color%}";              ;;
  esac
}

function mostly_return_status {
  echo "%(?:$(mostly_positive_return_code) %{$FG[254]%}[%{$FG[148]%}:$(mostly_negative_return_code) %{$FG[254]%}[%{$FG[160]%})%?%{$FG[254]%}]%{$reset_color%}"
}

function mostly_battery_charge {
  mains=0
  tester=$(ls /sys/class/power_supply/BAT0 2> /dev/null) || mains=1
  if [[ 1 == $mains ]]; then
    echo -n "%{$FG[254]%}[ %{$FG[197]%}∞%{$FG[254]%} ]%{$reset_color%}"
  else
    b_now=$(cat /sys/class/power_supply/BAT0/charge_now)
    b_full=$(cat /sys/class/power_supply/BAT0/charge_full)
    b_status=$(cat /sys/class/power_supply/BAT0/status)
    # Prevent issues with differences between charge_full and charge_full_design
    if [ "$b_full" -lt "$b_now" ]; then
      b_now=$b_full
    fi
    # I am displaying 10 chars -> charge is in {0..9}
    charge=$(expr $(expr $b_now \* 10) / $b_full)

    # Echo the backets and battery label
    echo -n "%{$FG[254]%}Battery [%{$reset_color%}"

    # choose the color according the charge or if we are charging then always green
    if [[ "Charging" == $b_status ]]; then
      echo -n "%{$FG[081]%}"
    else
      if [[ charge -gt 6 || "Charging" == $b_status ]]; then
        echo -n "%{$FG[148]%}"
      elif [[ charge -gt 3 ]]; then
        echo -n "%{$FG[208]%}"
      else
        echo -n "%{$FG[160]%}"
      fi
    fi
  
    # display charge * '▸' and (10 - charge) * '▹'
    i=0;
    while [[ i -lt $charge ]]
    do
      i=$(expr $i + 1)
      echo -n "■"
    done
    while [[ i -lt 8 ]]
    do
      i=$(expr $i + 1)
      echo -n "□"
    done

    echo -n "%{$reset_color%}"
  
    # display a plus if we are charging
    if [[ "Charging" == $b_status ]]; then
      echo -n "%{$FG_BOLD[081]%} +%{$reset_color%}"
    fi

    # Close brackets and label
    echo -n "%{$FG[254]%}]%{$reset_color%}"
  fi
}

function mostly_vi_status() {
  if {echo $fpath | grep -q "plugins/vi-mode"}; then
    echo "$(vi_mode_prompt_info)"
  fi
}

function mostly_user_host() {
  if [[ $UID -eq 0 ]]; then
      echo "%{$FG[160]%}%n%{$FG[177]%}@%{$FG[208]%}%m%{$reset_color%}"
  else
      echo "%{$FG[197]%}%n%{$FG[177]%}@%{$FG[208]%}%m%{$reset_color%}"
  fi
}

function mostly_user_symbol() {
  if [[ $UID -eq 0 ]]; then
      echo "#"
  else
      echo "$"
  fi
} 

function mostly_ssh_connection() {
  if [[ -n $SSH_CONNECTION ]]; then
    echo "%{$FG[160]%}(ssh)%{$reset_color%} "
  fi
}

function mostly_path_location() {
    echo "%{$FG[148]%}%U%~%u%{$reset_color%}"
}

PROMPT='╭─$(mostly_ssh_connection)$(mostly_user_host) : $(mostly_path_location)$(mostly_git_prompt)
╰─$(mostly_battery_charge)$(mostly_user_symbol)'
RPROMPT='$(mostly_vi_status) $(mostly_return_status)'


     
